export default function createRect(fillcolor, bordercolor, len, angle) {
    let figure = new PIXI.Graphics();
    figure.beginFill(fillcolor);
    figure.lineStyle(4, bordercolor, 1);
    figure.drawRect(-len/2, -len/2, len, len);
    figure.figuretype = "rect";
    figure.rotation = angle;
    figure.endFill();
    return figure;
}

export function createCircle(fillcolor, bordercolor, len, angle) {
    let figure = new PIXI.Graphics();
    figure.beginFill(fillcolor);
    figure.lineStyle(4, bordercolor, 1);
    figure.drawCircle(0, 0, len/2, len/2);
    figure.figuretype = "circle";
    figure.rotation = angle;
    figure.endFill();
    return figure;
}

export function createTriangle(fillcolor, bordercolor, len, angle) {
    let figure = new PIXI.Graphics();
    figure.beginFill(fillcolor);
    figure.lineStyle(4, bordercolor, 1);
    figure.drawPolygon([
        -len*2, len,             
        len, len,              
        len, -len*2                 
    ]);
    figure.figuretype = "triangle";
    figure.rotation = angle;
    figure.endFill();
    return figure;
}

