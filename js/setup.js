import createRect, { createCircle, createTriangle } from "./figures.js";

function CreateGame(id) {
    const canvas = document.getElementById(id);

    let app = new PIXI.Application({
        view: canvas,
        width: 1200,
        height: 700,
        antialias: true, 
        transparent: false, 
        resolution: 1
    });

    let rectangle = new PIXI.Graphics();
        rectangle.lineStyle(4, 0xFF3300, 1);
        rectangle.drawRect(0, 0, 100, 100);
        rectangle.endFill();
        rectangle.x = 150;
        rectangle.y = 700 - rectangle.height;
        rectangle.pivot.set(50, 50);

        rectangle.figuretype = "rect";

    let circle = new PIXI.Graphics();
        circle.lineStyle(4, 0xFF3300, 1);
        circle.drawCircle(0, 0, 50);
        circle.endFill();
        circle.x = 600;
        circle.y = 590;

        circle.figuretype = "circle";

    let triangle = new PIXI.Graphics();
        triangle.lineStyle(4, 0xFF3300, 1);
        triangle.drawPolygon([-60, 80, 60, 80, 0, 0]);
        triangle.endFill();
        triangle.x = 1010;
        triangle.y = 610;
        triangle.pivot.set(0, 60);

        triangle.figuretype = "triangle";

    let line = new PIXI.Graphics();
        line.lineStyle(4, 0xFFFFFF, 1);
        line.moveTo(0, 0);
        line.lineTo(1200, 0);
        line.x = 0;
        line.y = 500;

    app.stage.addChild(line);
    app.stage.addChild(rectangle);
    app.stage.addChild(circle);
    app.stage.addChild(triangle);

    function createFigure() {
        let typefig = getRandomInt(1, 4);
        console.log(typefig)
        let figure;

        switch (typefig) {
            case 1: 
                figure = new createRect(getRandomColorVBHex(), getRandomColorVBHex(), getRandomInt(), getRandomInt(1, 10));
                break;
            case 2: 
                figure = new createCircle(getRandomColorVBHex(), getRandomColorVBHex(), getRandomInt(), getRandomInt(1, 10));
                break;
            case 3: 
                figure = new createTriangle(getRandomColorVBHex(), getRandomColorVBHex(), getRandomInt()/2, getRandomInt(1, 10));
        }

        return figure;
    }   

    this.renderFigures = function() {
        let countFigs = 8;
        let spacing = 80;
        let xOffset = 150;
        
        for (let i=0; i<countFigs; i++) {
            
            let figure = createFigure(1);
            let x = xOffset * i + spacing
            let y = getRandomInt(100, 400);

            figure.x = x;
            figure.y = y;

            figure.interactive = true;
            figure.buttonMode = true;
            figure.on('pointerdown', onDragStart)
                  .on('pointerup', onDragEnd)
                  .on('pointerupoutside', onDragEnd)
                  .on('pointermove', onDragMove);

            app.stage.addChild(figure);
        }
    }

    function onDragStart(event) {
        this.data = event.data;
        this.initialx = this.data.global.x;
        this.initialy = this.data.global.y;
        this.dragging = true;
        // this.scale.set(1.3, 1.3);
    }

    function onDragEnd() {
        this.alpha = 1;
        this.dragging = false;
        this.scale.set(1,1);
        isOverlay(this);
    }
    
    function onDragMove() {
        if (this.dragging) {
            const newPosition = this.data.getLocalPosition(this.parent);
            if ((newPosition.x > 0 &&  newPosition.x < 1200) &&
                (newPosition.y > 0 &&  newPosition.y < 700)) {
                    this.x = newPosition.x;
                    this.y = newPosition.y;
                }   
        }
    }
    
    function isOverlay(figure){
        isHit(figure, rectangle);
        isHit(figure, circle);
        isHit(figure, triangle);

        function isHit(fig, rezfig) {
            console.log(rectangle.figuretype)
            let combinedHalfWidths, combinedHalfHeights, vx, vy;

            console.log(fig.halfWidth);

            fig.centerX = fig.x + fig.width / 2;
            fig.centerY = fig.y + fig.height / 2;
            rezfig.centerX = rezfig.x + rezfig.width / 2;
            rezfig.centerY = rezfig.y + rezfig.height / 2;
          
            fig.halfWidth = fig.width / 2;
            fig.halfHeight = fig.height / 2;
            rezfig.halfWidth = rezfig.width / 2;
            rezfig.halfHeight = rezfig.height / 2;
          
            vx = fig.centerX - rezfig.centerX;
            vy = fig.centerY - rezfig.centerY;
          
            combinedHalfWidths = fig.halfWidth + rezfig.halfWidth;
            combinedHalfHeights = fig.halfHeight + rezfig.halfHeight;
          
            if ((Math.abs(vx) < combinedHalfWidths) && (Math.abs(vy) < combinedHalfHeights)) {
                if (figure.figuretype == rezfig.figuretype) {
                    figure.visible = false;
                }
                else {
                    figure.x = figure.initialx;
                    figure.y = figure.initialy;
                }
            }
          };
    }
    //
    function getRandomInt(min = 50, max = 100) {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    function getRandomColorVBHex() {
        let range = "1234567890ABCDEF";
        let result = "0x";
        for (let i=0; i<6; i++) {
            result += range[getRandomInt(0, 15)];
        }
        return result;
    }

    document.body.appendChild(app.view);
}

export default CreateGame;